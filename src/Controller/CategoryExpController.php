<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use App\Entity\CategoryExp;

class CategoryExpController extends AbstractController
{
    public function getAll(): JsonResponse
    {
        $entities = $this->getDoctrine()->getRepository(CategoryExp::class)->findAll();
        $data = [];

        foreach ($entities as $entity) {
            $data[] = [
                'id' => $entity->getId(),
                'name' => $entity->getName(),
                'description' => $entity->getDescription(),
            ];
        }

        return new JsonResponse($data, Response::HTTP_OK);
    }

    public function getOne($id): JsonResponse
    {
        $entity = $this->getDoctrine()->getRepository(CategoryExp::class)->findOneBy(['id' => $id]);

        $data = [
            'id' => $entity->getId(),
            'name' => $entity->getName(),
            'description' => $entity->getDescription(),
        ];

        return new JsonResponse($data, Response::HTTP_OK);
    }

    public function add(Request $request): JsonResponse
    {
        
        $json = $request->getContent();
        $params = json_decode($json);

        if ($json != null) {
            
            $name = (!empty($params->name)) ? $params->name : null;
            $description = (!empty($params->description)) ? $params->description : null;
            
            if (
                !empty($name) && 
                !empty($description)
            ) {
                $entity = new CategoryExp();
                $entity
                    ->setName($name)
                    ->setDescription($description)
                ;

                $em = $this->getDoctrine()->getManager();
                $em->persist($entity);
                $em->flush();
                
                $data = [
                    'status' => 'success',
                    'code' => 200,
                    'message' => 'Elemento creado.'
                ];

            } else {
                $data = [
                    'status' => 'error',
                    'code' => 400,
                    'message' => 'El elemento no se ha podido crear.'
                ];
            }

        } 

        return new JsonResponse($data);
    }

    public function update($id, Request $request): JsonResponse
    {
        
        $json = $request->getContent();
        $params = json_decode($json);

        if ($json != null) {
            
            $name = (!empty($params->name)) ? $params->name : null;
            $description = (!empty($params->description)) ? $params->description : null;
            
            if (
                !empty($name) && 
                !empty($description)
            ) {
                
                $entity = $this->getDoctrine()->getRepository(CategoryExp::class)->findOneBy(['id' => $id]);
                $entity
                    ->setName($name)
                    ->setDescription($description)
                ;

                $em = $this->getDoctrine()->getManager();
                $em->persist($entity);
                $em->flush();
                
                $data = [
                    'status' => 'success',
                    'code' => 200,
                    'message' => 'Elemento actualizado.'
                ];

            } else {
                $data = [
                    'status' => 'error',
                    'code' => 400,
                    'message' => 'El elemento no se ha podido actualizar.'
                ];
            }

        } 

        return new JsonResponse($data);
    }

    public function delete($id): JsonResponse
    {
        $entity = $this->getDoctrine()->getRepository(CategoryExp::class)->findOneBy(['id' => $id]);

        $em = $this->getDoctrine()->getManager();
        $em->remove($entity);
        $em->flush();

        return new JsonResponse(['status' => 'Element deleted!'], Response::HTTP_OK);
    }
}