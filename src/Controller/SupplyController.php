<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use App\Entity\Supply;
use App\Entity\CategoryExp;

class SupplyController extends AbstractController
{
    public function getAll(): JsonResponse
    {
        $entities = $this->getDoctrine()->getRepository(Supply::class)->findAll();
        $data = [];

        foreach ($entities as $entity) {
            $data[] = [
                'id' => $entity->getId(),
                'name' => $entity->getName(),
                'description' => $entity->getDescription(),
                'stock' => $entity->getStock(),
                'price' => $entity->getPrice(),
                'type' => $entity->getType(),
                'category' => $entity->getCategory(),
                'active' => $entity->getActive(),
                'created_at' => $entity->getCreatedAt(),
                'updated_at' => $entity->getUpdatedAt(),
            ];
        }

        return new JsonResponse($data, Response::HTTP_OK);
    }

    public function getOne($id): JsonResponse
    {
        $entity = $this->getDoctrine()->getRepository(Supply::class)->findOneBy(['id' => $id]);

        $data = [
            'id' => $entity->getId(),
            'name' => $entity->getName(),
            'description' => $entity->getDescription(),
            'stock' => $entity->getStock(),
            'price' => $entity->getPrice(),
            'type' => $entity->getType(),
            'category' => $entity->getCategory(),
            'active' => $entity->getActive(),
            'created_at' => $entity->getCreatedAt(),
            'updated_at' => $entity->getUpdatedAt(),
        ];

        return new JsonResponse($data, Response::HTTP_OK);
    }

    public function add(Request $request): JsonResponse
    {
        
        $json = $request->getContent();
        $params = json_decode($json);

        if ($json != null) {
            
            $name = (!empty($params->name)) ? $params->name : null;
            $description = (!empty($params->description)) ? $params->description : null;
            $stock = (!empty($params->stock)) ? $params->stock : null;
            $price = (!empty($params->price)) ? $params->price : "0.00";
            $type = (!empty($params->type)) ? $params->type : 0;
            $category = (!empty($params->category)) ? $params->category : "-";
            $active = (!empty($params->active)) ? $params->active : false;
            $created = (!empty($params->created_at)) ? $params->created_at : null;

            if (
                !empty($name) && 
                !empty($description) &&
                !empty($stock) &&
                !empty($category) &&
                !empty($active) &&
                !empty($price)
            ) {
                $em =  $this->getDoctrine()->getManager();

                if ($created) {
                    $created_at = new \DateTime($created);
                } else {
                    $created_at = new \DateTime("now");
                }
                
                $updated_at = new \DateTime("now");
                
                $entity = new Supply();
                $entity
                    ->setName($name)
                    ->setDescription($description)
                    ->setPrice($price)
                    ->setStock($stock)
                    ->setType($type)
                    ->setActive($active)
                    ->setCreatedAt($created_at)
                    ->setUpdatedAt($updated_at)
                ;

                if ($category != "-") {
                    $categoryExp = $this->getDoctrine()->getRepository(CategoryExp::class)->findOneBy(['id' => $category->id]);
                    $entity->setCategory($categoryExp);
                } else {
                    $categoryExp = $this->getDoctrine()->getRepository(CategoryExp::class)->find(8);
                    $entity->setCategory($categoryExp);
                }

                $em->persist($entity);
                $em->flush();
                
                $data = [
                    'status' => 'success',
                    'code' => 200,
                    'message' => 'Elemento creado.'
                ];

            } else {
                $data = [
                    'status' => 'error',
                    'code' => 400,
                    'message' => 'El elemento no se ha podido crear.'
                ];
            }

        } 

        return new JsonResponse($data);
    }

    public function update($id, Request $request): JsonResponse
    {
        
        $json = $request->getContent();
        $params = json_decode($json);

        if ($json != null) {
            
            $name = (!empty($params->name)) ? $params->name : null;
            $description = (!empty($params->description)) ? $params->description : null;
            $stock = (!empty($params->stock)) ? $params->stock : 0;
            $price = (!empty($params->price)) ? $params->price : "0.00";
            $type = (!empty($params->type)) ? $params->type : 0;
            $category = (!empty($params->category)) ? $params->category : "-";
            $active = (!empty($params->active)) ? $params->active : false;
            $created = (!empty($params->created_at)) ? $params->created_at : null;

            if (
                !empty($name) && 
                !empty($description) &&
                !empty($category) &&
                !empty($price) 
            ) {
                $em =  $this->getDoctrine()->getManager();

                $updated_at = new \DateTime("now");

                $entity = $this->getDoctrine()->getRepository(Supply::class)->findOneBy(['id' => $id]);
                $entity
                    ->setName($name)
                    ->setDescription($description)
                    ->setStock($stock)
                    ->setPrice($price)
                    ->setType($type)
                    ->setActive($active)
                    ->setUpdatedAt($updated_at)
                ;

                if ($created) {
                    $created_at = new \DateTime($created);
                    $entity->setCreatedAt($created_at);
                }

                if ($category != "-") {
                    $categoryExp = $this->getDoctrine()->getRepository(CategoryExp::class)->findOneBy(['id' => $category->id]);
                    $entity->setCategory($categoryExp);
                } else {
                    $categoryExp = $this->getDoctrine()->getRepository(CategoryExp::class)->find(8);
                    $entity->setCategory($categoryExp);
                }

                $em->persist($entity);
                $em->flush();
                
                $data = [
                    'status' => 'success',
                    'code' => 200,
                    'message' => 'Elemento actualizado.'
                ];

            } else {
                $data = [
                    'status' => 'error',
                    'code' => 400,
                    'message' => 'El elemento no se ha podido actualizar.'
                ];
            }

        } 

        return new JsonResponse($data);
    }

    public function delete($id): JsonResponse
    {
        $entity = $this->getDoctrine()->getRepository(Supply::class)->findOneBy(['id' => $id]);

        $em = $this->getDoctrine()->getManager();
        $em->remove($entity);
        $em->flush();

        return new JsonResponse(['status' => 'Element deleted!'], Response::HTTP_OK);
    }
}