<?php
namespace App\Controller;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Constraints\Email;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use App\Entity\User;
use App\Entity\Client;
use App\Entity\Zone;
use App\Entity\ClientStatus;
use App\Entity\ClientOrigin;

use App\Repository\ClientRepository;

/**
 * Class ClientController
 * @package App\Controller
 *
 * @Route(path="/api/clients")
 */
class ClientController extends AbstractController
{
    private $repository;

    public function __construct(ClientRepository $repository)
    {
        $this->repository = $repository;
    }

    public function getAll(): JsonResponse
    {
        $entities = $this->getDoctrine()->getRepository(Client::class)->findAll();
        $data = [];

        foreach ($entities as $entity) {
            $data[] = [
                'id' => $entity->getId(),
                'firstname' => $entity->getFirstname(),
                'lastname' => $entity->getLastname(),
                'dni' => $entity->getDni(),
                'celphone' => $entity->getCelphone(),
                'address' => $entity->getAddress(),
                'email' => $entity->getEmail(),
                'debe' => $entity->getDebe(),
                'seller' => $entity->getSeller(),
                'province' => $entity->getProvince(),
                'locality' => $entity->getLocality(),
                'cp' => $entity->getCp(),
                'zone' => $entity->getZone(),
                'status' => $entity->getStatus(),
                'origin' => $entity->getOrigin(),
                'description' => $entity->getDescription(),
                'orders' => $entity->getOrders()->toArray(),
                'high_date' => $entity->getHighDate(),
                'created_at' => $entity->getCreatedAt(),
                'updated_at' => $entity->getUpdatedAt(),
            ];
        }

        return new JsonResponse($data, Response::HTTP_OK);
    }

    public function getAllZoned(): JsonResponse
    {
        //trae la zona como string y no como obj a diferencia del getAll

        $entities = $this->getDoctrine()->getRepository(Client::class)->findAll();
        $data = [];

        foreach ($entities as $entity) {
            $data[] = [
                'id' => $entity->getId(),
                'firstname' => $entity->getFirstname(),
                'lastname' => $entity->getLastname(),
                'dni' => $entity->getDni(),
                'celphone' => $entity->getCelphone(),
                'address' => $entity->getAddress(),
                'email' => $entity->getEmail(),
                'debe' => $entity->getDebe(),
                'seller' => $entity->getSeller(),
                'province' => $entity->getProvince(),
                'locality' => $entity->getLocality(),
                'cp' => $entity->getCp(),
                'zone' => $entity->getZone()->getName(),
                'status' => $entity->getStatus(),
                'origin' => $entity->getOrigin(),
                'description' => $entity->getDescription(),
                'orders' => $entity->getOrders()->toArray(),
                'high_date' => $entity->getHighDate(),
                'created_at' => $entity->getCreatedAt(),
                'updated_at' => $entity->getUpdatedAt(),
            ];
        }

        return new JsonResponse($data, Response::HTTP_OK);
    }

    public function getOrderBy(): JsonResponse
    {
        $dql = "
            SELECT c
            FROM App\Entity\Client c
            ORDER BY c.lastname, c.firstname
        ";

        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery($dql);

        $entities = $query->getResult();

        $data = [];

        foreach ($entities as $entity) {
            $data[] = [
                'id' => $entity->getId(),
                'firstname' => $entity->getFirstname(),
                'lastname' => $entity->getLastname(),
                'dni' => $entity->getDni(),
                'celphone' => $entity->getCelphone(),
                'address' => $entity->getAddress(),
                'email' => $entity->getEmail(),
                'debe' => $entity->getDebe(),
                'seller' => $entity->getSeller(),
                'province' => $entity->getProvince(),
                'locality' => $entity->getLocality(),
                'cp' => $entity->getCp(),
                'zone' => $entity->getZone(),
                'status' => $entity->getStatus(),
                'origin' => $entity->getOrigin(),
                'description' => $entity->getDescription(),
                'orders' => $entity->getOrders()->toArray(),
                'high_date' => $entity->getHighDate(),
                'created_at' => $entity->getCreatedAt(),
                'updated_at' => $entity->getUpdatedAt(),
            ];
        }

        return new JsonResponse($data, Response::HTTP_OK);
    }

    public function getOne($id): JsonResponse
    {
        $entity = $this->getDoctrine()->getRepository(Client::class)->findOneBy(['id' => $id]);

        $data = [
            'id' => $entity->getId(),
            'firstname' => $entity->getFirstname(),
            'lastname' => $entity->getLastname(),
            'dni' => $entity->getDni(),
            'celphone' => $entity->getCelphone(),
            'address' => $entity->getAddress(),
            'email' => $entity->getEmail(),
            'debe' => $entity->getDebe(),
            'seller' => $entity->getSeller(),
            'province' => $entity->getProvince(),
            'locality' => $entity->getLocality(),
            'cp' => $entity->getCp(),
            'zone' => $entity->getZone(),
            'status' => $entity->getStatus(),
            'origin' => $entity->getOrigin(),
            'description' => $entity->getDescription(),
            'orders' => $entity->getOrders()->toArray(),
            'high_date' => $entity->getHighDate(),
            'created_at' => $entity->getCreatedAt(),
            'updated_at' => $entity->getUpdatedAt(),
        ];

        return new JsonResponse($data, Response::HTTP_OK);
    }

    public function add(Request $request): JsonResponse
    {
        
        $json = $request->getContent();
        $params = json_decode($json);

        if ($json != null) {
            
            $firstname = (!empty($params->firstname)) ? $params->firstname : null;
            $lastname = (!empty($params->lastname)) ? $params->lastname : null;
            $dni = (!empty($params->dni)) ? $params->dni : null;
            $celphone = (!empty($params->celphone)) ? $params->celphone : null;
            $address = (!empty($params->address)) ? $params->address : null;
            $email = (!empty($params->email)) ? $params->email : null;
            $seller = (!empty($params->seller)) ? $params->seller : null;
            $province = (!empty($params->province)) ? $params->province : null;
            $locality = (!empty($params->locality)) ? $params->locality : null;
            $cp = (!empty($params->cp)) ? $params->cp : null;
            $zone = (!empty($params->zone)) ? $params->zone : null;
            $status = (!empty($params->status)) ? $params->status : null;
            $origin = (!empty($params->origin)) ? $params->origin : null;
            $description = (!empty($params->description)) ? $params->description : null;
            $debe = (!empty($params->debe)) ? $params->debe : "0.00";
            $high_date = (!empty($params->high_date)) ? $params->high_date : null;
            
            if (
                !empty($firstname) && 
                !empty($lastname) &&
                !empty($dni) &&
                !empty($celphone) &&
                !empty($address) &&
                !empty($email) &&
                !empty($seller) &&
                !empty($province) &&
                !empty($locality) &&
                !empty($cp) &&
                !empty($zone) &&
                !empty($status) &&
                !empty($origin) &&
                !empty($description) &&
                !empty($debe) &&
                !empty($high_date)
            ) {
                $created_at = new \DateTime("now");
                $updated_at = new \DateTime("now");
                $high_date = new \DateTime($high_date);

                $client_zone = $this->getDoctrine()->getRepository(Zone::class)->find($zone);
                $client_status = $this->getDoctrine()->getRepository(ClientStatus::class)->find($status);
                $client_origin = $this->getDoctrine()->getRepository(ClientOrigin::class)->find($origin);
                $seller = $this->getDoctrine()->getRepository(User::class)->find($seller);

                $entity = new Client();
                $entity
                    ->setFirstname($firstname)
                    ->setLastname($lastname)
                    ->setDni($dni)
                    ->setCelphone($celphone)
                    ->setAddress($address)
                    ->setEmail($email)
                    ->setDebe($debe)
                    ->setSeller($seller)
                    ->setProvince($province)
                    ->setLocality($locality)
                    ->setCp($cp)
                    ->setZone($client_zone)
                    ->setStatus($client_status)
                    ->setOrigin($client_origin)
                    ->setDescription($description)
                    ->setHighDate($high_date)
                    ->setCreatedAt($created_at)
                    ->setUpdatedAt($updated_at)
                ;

                $em = $this->getDoctrine()->getManager();
                $em->persist($entity);

                $exist = $this->getDoctrine()->getRepository(Client::class)->findOneBy(['dni' => $dni]);
                if ($exist) {
                    $data = [
                        'status' => 'error',
                        'code' => 400,
                        'message' => 'Ya existe un cliente con ese DNI.',
                        'params' => $exist
                    ];
                } else {
                    $em->flush();
                
                    $data = [
                        'status' => 'success',
                        'code' => 200,
                        'message' => 'Elemento creado.'
                    ];
                }

            } else {
                $data = [
                    'status' => 'error',
                    'code' => 400,
                    'message' => 'El elemento no se ha podido crear.',
                    'params' => $debe
                ];
            }

        } 

        return new JsonResponse($data);
    }

    public function update($id, Request $request): JsonResponse
    {
        
        $json = $request->getContent();
        $params = json_decode($json);

        if ($json != null) {
            
            $firstname = (!empty($params->firstname)) ? $params->firstname : null;
            $lastname = (!empty($params->lastname)) ? $params->lastname : null;
            $dni = (!empty($params->dni)) ? $params->dni : null;
            $celphone = (!empty($params->celphone)) ? $params->celphone : null;
            $address = (!empty($params->address)) ? $params->address : null;
            $email = (!empty($params->email)) ? $params->email : null;
            $seller = (!empty($params->seller)) ? $params->seller : null;
            $province = (!empty($params->province)) ? $params->province : null;
            $locality = (!empty($params->locality)) ? $params->locality : null;
            $cp = (!empty($params->cp)) ? $params->cp : null;
            $zone = (!empty($params->zone)) ? $params->zone : null;
            $status = (!empty($params->status)) ? $params->status : null;
            $origin = (!empty($params->origin)) ? $params->origin : null;
            $description = (!empty($params->description)) ? $params->description : null;
            $debe = (!empty($params->debe)) ? $params->debe : "0.00";
            $high_date = (!empty($params->high_date)) ? $params->high_date : null;
            
            if (
                !empty($firstname) && 
                !empty($lastname) &&
                !empty($dni) &&
                !empty($celphone) &&
                !empty($address) &&
                !empty($seller) &&
                !empty($email) &&
                !empty($province) &&
                !empty($locality) &&
                !empty($cp) &&
                !empty($zone) &&
                !empty($status) &&
                !empty($origin) &&
                !empty($description) &&
                !empty($debe) &&
                !empty($high_date)
            ) {
                
                $updated_at = new \DateTime("now");
                $high_date = new \DateTime($high_date);

                $client_zone = $this->getDoctrine()->getRepository(Zone::class)->find($zone);
                $client_status = $this->getDoctrine()->getRepository(ClientStatus::class)->find($status);
                $client_origin = $this->getDoctrine()->getRepository(ClientOrigin::class)->find($origin);
                $seller = $this->getDoctrine()->getRepository(User::class)->find($seller);

                $entity = $this->getDoctrine()->getRepository(Client::class)->findOneBy(['id' => $id]);
                $entity
                    ->setFirstname($firstname)
                    ->setLastname($lastname)
                    ->setDni($dni)
                    ->setCelphone($celphone)
                    ->setAddress($address)
                    ->setEmail($email)
                    ->setSeller($seller)
                    ->setDebe($debe)
                    ->setProvince($province)
                    ->setLocality($locality)
                    ->setCp($cp)
                    ->setZone($client_zone)
                    ->setStatus($client_status)
                    ->setOrigin($client_origin)
                    ->setDescription($description)
                    ->setHighDate($high_date)
                    ->setUpdatedAt($updated_at)
                ;

                $em = $this->getDoctrine()->getManager();
                $em->persist($entity);
                $em->flush();
                
                $data = [
                    'status' => 'success',
                    'code' => 200,
                    'message' => 'Elemento actualizado.'
                ];

            } else {
                $data = [
                    'status' => 'error',
                    'code' => 400,
                    'message' => 'El elemento no se ha podido actualizar.'
                ];
            }

        } 

        return new JsonResponse($data);
    }

    public function delete($id): JsonResponse
    {
        $entity = $this->getDoctrine()->getRepository(Client::class)->findOneBy(['id' => $id]);

        $em = $this->getDoctrine()->getManager();
        $em->remove($entity);
        $em->flush();

        return new JsonResponse(['status' => 'Element deleted!'], Response::HTTP_OK);
    }
}