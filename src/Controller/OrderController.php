<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use App\Entity\User;
use App\Entity\Order;
use App\Entity\Client;
use App\Entity\Supply;
use App\Entity\OrderSupply;

class OrderController extends AbstractController
{
    public function getAll(): JsonResponse
    {
        $entities = $this->getDoctrine()->getRepository(Order::class)->findAll();
        $data = [];

        foreach ($entities as $entity) {
            $data[] = [
                'id' => $entity->getId(),
                'client' => $entity->getClient(),
                'seller' => $entity->getSeller(),
                'supply' => $entity->getSupply(),
                'supplies' => $entity->getSupplies()->toArray(),
                'address' => $entity->getAddress(),
                'delivery' => $entity->getDelivery(),
                'total' => $entity->getTotal(),
                'status' => $entity->getStatus(),
                'quantity' => $entity->getQuantity(),
                'description' => $entity->getDescription(),
                'delivery_date' => $entity->getDeliveryDate(),
                'load_date' => $entity->getLoadDate(),
                'created_at' => $entity->getCreatedAt(),
                'updated_at' => $entity->getUpdatedAt(),
            ];
        }

        return new JsonResponse($data, Response::HTTP_OK);
    }

    public function getAllWithNames(): JsonResponse
    {
        $entities = $this->getDoctrine()->getRepository(Order::class)->findAll();
        $data = [];

        foreach ($entities as $entity) {
            $data[] = [
                'id' => $entity->getId(),
                'client' => $entity->getClient(),
                'client_firstname' => $entity->getClient()->getFirstName(),
                'client_lastname' => $entity->getClient()->getLastName(),
                'seller' => $entity->getSeller(),
                'supply' => $entity->getSupply(),
                'supplies' => $entity->getSupplies()->toArray(),
                'address' => $entity->getAddress(),
                'delivery' => $entity->getDelivery(),
                'total' => $entity->getTotal(),
                'status' => $entity->getStatus(),
                'quantity' => $entity->getQuantity(),
                'description' => $entity->getDescription(),
                'delivery_date' => $entity->getDeliveryDate(),
                'load_date' => $entity->getLoadDate(),
                'created_at' => $entity->getCreatedAt(),
                'updated_at' => $entity->getUpdatedAt(),
            ];
        }

        return new JsonResponse($data, Response::HTTP_OK);
    }

    public function getOne($id): JsonResponse
    {
        $entity = $this->getDoctrine()->getRepository(Order::class)->findOneBy(['id' => $id]);

        $data = [
            'id' => $entity->getId(),
            'client' => $entity->getClient(),
            'seller' => $entity->getSeller(),
            'supply' => $entity->getSupply(),
            'supplies' => $entity->getSupplies()->toArray(),
            'delivery' => $entity->getDelivery(),
            'address' => $entity->getAddress(),
            'total' => $entity->getTotal(),
            'status' => $entity->getStatus(),
            'quantity' => $entity->getQuantity(),
            'description' => $entity->getDescription(),
            'delivery_date' => $entity->getDeliveryDate(),
            'load_date' => $entity->getLoadDate(),
            'created_at' => $entity->getCreatedAt(),
            'updated_at' => $entity->getUpdatedAt(),
        ];

        return new JsonResponse($data, Response::HTTP_OK);
    }

    public function add(Request $request): JsonResponse
    {
        
        $json = $request->getContent();
        $params = json_decode($json);

        if ($json != null) {
            
            $client = (!empty($params->client)) ? $params->client : null;
            $seller = (!empty($params->seller)) ? $params->seller : null;
            // $supply = (!empty($params->supply)) ? $params->supply : null;
            $delivery = (!empty($params->delivery)) ? $params->delivery : null;
            $address = (!empty($params->address)) ? $params->address : null;
            $total = (!empty($params->total)) ? $params->total : null;
            $status = (!empty($params->status)) ? $params->status : null;
            // $quantity = (!empty($params->quantity)) ? $params->quantity : null;
            $description = (!empty($params->description)) ? $params->description : null;
            $supplies = (!empty($params->supplies)) ? $params->supplies : null;
            $created_at = (!empty($params->created_at)) ? $params->created_at : null;
            $delivery_date = (!empty($params->delivery_date)) ? $params->delivery_date : null;
            $load_date = (!empty($params->load_date)) ? $params->load_date : null;
            
            if (
                !empty($client) && 
                !empty($seller) &&
                // !empty($supply) &&
                !empty($delivery) &&
                !empty($address) &&
                !empty($total) &&
                !empty($status) &&
                !empty($description) &&
                // !empty($quantity)
                !empty($supplies) &&
                !empty($created_at) &&
                !empty($delivery_date) &&
                !empty($load_date)
            ) {
                $updated_at = new \DateTime("now");
                $created_at = new \DateTime($created_at);
                $deliv_date = new \DateTime($delivery_date);
                $load_date = new \DateTime($load_date);
                
                $clientEnt = $this->getDoctrine()->getRepository(Client::class)->findOneBy(['id' => $client]);
                $sellerEnt = $this->getDoctrine()->getRepository(User::class)->findOneBy(['id' => $seller]);
                $deliveryEnt = $this->getDoctrine()->getRepository(User::class)->findOneBy(['id' => $delivery]);
                // $supplyEnt = $this->getDoctrine()->getRepository(Supply::class)->findOneBy(['id' => $supply->id]);

                $entity = new Order();
                $entity
                    ->setClient($clientEnt)
                    ->setSeller($sellerEnt)
                    ->setDelivery($deliveryEnt)
                    // ->setSupply($supplyEnt)
                    ->setAddress($address)
                    ->setTotal($total)
                    ->setStatus($status)
                    // ->setQuantity($quantity)
                    ->setDescription($description)
                    ->setDeliveryDate($deliv_date)
                    ->setLoadDate($load_date)
                    ->setCreatedAt($created_at)
                    ->setUpdatedAt($updated_at)
                ;

                if ($supplies) {
                    foreach ($supplies as $key => $supply) {
                        $orderId = $entity;
                        $supplyId = $this->getDoctrine()->getRepository(Supply::class)->find($supply->supply->id);
                        $quantity = $supply->quantity;

                        $orderSupply = new OrderSupply();
                        $orderSupply
                            ->setOrder($orderId)
                            ->setSupply($supplyId)
                            ->setQuantity($quantity)
                        ;

                        $entity->addSupply($orderSupply);
                    }
                }

                $em =  $this->getDoctrine()->getManager();
                $em->persist($entity);
                $clientEnt->addOrder($entity);
                $em->persist($clientEnt);
                $em->flush();
                
                $data = [
                    'status' => 'success',
                    'code' => 200,
                    'message' => 'Elemento creado.'
                ];

            } else {
                $data = [
                    'status' => 'error',
                    'code' => 400,
                    'message' => 'El elemento no se ha podido crear.'
                ];
            }

        } 

        return new JsonResponse($data);
    }

    public function update($id, Request $request): JsonResponse
    {
        
        $json = $request->getContent();
        $params = json_decode($json);

        if ($json != null) {

            $client = (!empty($params->client)) ? $params->client : null;
            $seller = (!empty($params->seller)) ? $params->seller : null;
            // $supply = (!empty($params->supply)) ? $params->supply : null;
            $delivery = (!empty($params->delivery)) ? $params->delivery : null;
            $address = (!empty($params->address)) ? $params->address : null;
            $total = (!empty($params->total)) ? $params->total : null;
            $status = (!empty($params->status)) ? $params->status : null;
            // $quantity = (!empty($params->quantity)) ? $params->quantity : null;
            $description = (!empty($params->description)) ? $params->description : null;
            $supplies = (!empty($params->supplies)) ? $params->supplies : null;
            $created_at = (!empty($params->created_at)) ? $params->created_at : null;
            $delivery_date = (!empty($params->delivery_date)) ? $params->delivery_date : null;
            $load_date = (!empty($params->load_date)) ? $params->load_date : null;

            if (
                !empty($client) && 
                !empty($seller) &&
                // !empty($supply) &&
                !empty($delivery) &&
                !empty($description) &&
                !empty($address) &&
                !empty($total) &&
                !empty($status) &&
                !empty($supplies) &&
                !empty($created_at) &&
                !empty($delivery_date) &&
                !empty($load_date)
                // !empty($quantity)
            ) {

                $updated_at = new \DateTime("now");
                $created_at = new \DateTime($created_at);
                $deliv_date = new \DateTime($delivery_date);
                $load_date = new \DateTime($load_date);

                $clientEnt = $this->getDoctrine()->getRepository(Client::class)->findOneBy(['id' => $client]);
                $sellerEnt = $this->getDoctrine()->getRepository(User::class)->findOneBy(['id' => $seller]);
                $deliveryEnt = $this->getDoctrine()->getRepository(User::class)->findOneBy(['id' => $delivery]);
                // $supplyEnt = $this->getDoctrine()->getRepository(Supply::class)->findOneBy(['id' => $supply->id]);

                $entity = $this->getDoctrine()->getRepository(Order::class)->findOneBy(['id' => $id]);
                $entity
                    ->setClient($clientEnt)
                    ->setSeller($sellerEnt)
                    ->setDelivery($deliveryEnt)
                    // ->setSupply($supplyEnt)
                    ->setAddress($address)
                    ->setTotal($total)
                    ->setDescription($description)
                    ->setStatus($status)
                    // ->setQuantity($quantity)
                    ->setCreatedAt($created_at)
                    ->setDeliveryDate($deliv_date)
                    ->setLoadDate($load_date)
                    ->setUpdatedAt($updated_at)
                ;
                
                $em = $this->getDoctrine()->getManager();

                if ($supplies) {
                    $entity->getSupplies()->clear();
                    $em->persist($entity);
                    $em->flush();
                    foreach ($supplies as $key => $supply) {
                        $orderId = $entity;
                        $supplyId = $this->getDoctrine()->getRepository(Supply::class)->find($supply->supply->id);
                        $quantity = $supply->quantity;

                        $orderSupply = new OrderSupply();
                        $orderSupply
                            ->setOrder($orderId)
                            ->setSupply($supplyId)
                            ->setQuantity($quantity)
                        ;

                        $entity->addSupply($orderSupply);
                    }
                }

                $em->persist($entity);
                $em->flush();
                
                $data = [
                    'status' => 'success',
                    'code' => 200,
                    'message' => 'Elemento actualizado.'
                ];

            } else {
                $data = [
                    'status' => 'error',
                    'code' => 400,
                    'message' => 'El elemento no se ha podido actualizar.'
                ];
            }

        } 

        return new JsonResponse($data);
    }

    public function delete($id): JsonResponse
    {
        $entity = $this->getDoctrine()->getRepository(Order::class)->findOneBy(['id' => $id]);

        $em = $this->getDoctrine()->getManager();
        $em->remove($entity);
        $em->flush();

        return new JsonResponse(['status' => 'Element deleted!'], Response::HTTP_OK);
    }

    public function sellOne($id): JsonResponse
    {
        $entity = $this->getDoctrine()->getRepository(Order::class)->findOneBy(['id' => $id]);

        $entity->setStatus('VENDIDO');
        
        $em =  $this->getDoctrine()->getManager();
        $em->persist($entity);
        $em->flush();

        $data = [
            'status' => 'success',
            'code' => 200,
            'message' => 'Elemento actualizado.'
        ];

        return new JsonResponse($data, Response::HTTP_OK);
    }

    public function finishOne($id): JsonResponse
    {
        $entity = $this->getDoctrine()->getRepository(Order::class)->findOneBy(['id' => $id]);

        $entity->setStatus('ENTREGADO');
        
        $em =  $this->getDoctrine()->getManager();
        $em->persist($entity);
        $em->flush();

        $data = [
            'status' => 'success',
            'code' => 200,
            'message' => 'Elemento actualizado.'
        ];

        return new JsonResponse($data, Response::HTTP_OK);
    }
}