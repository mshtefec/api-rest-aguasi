<?php

namespace App\Repository;

use App\Entity\ClientStatus;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @method ClientStatus|null find($id, $lockMode = null, $lockVersion = null)
 * @method ClientStatus|null findOneBy(array $criteria, array $orderBy = null)
 * @method ClientStatus[]    findAll()
 * @method ClientStatus[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ClientStatusRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry, EntityManagerInterface $manager)
    {
        parent::__construct($registry, ClientStatus::class);
        $this->manager = $manager;
    }
}
