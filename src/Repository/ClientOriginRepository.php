<?php

namespace App\Repository;

use App\Entity\ClientOrigin;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @method ClientOrigin|null find($id, $lockMode = null, $lockVersion = null)
 * @method ClientOrigin|null findOneBy(array $criteria, array $orderBy = null)
 * @method ClientOrigin[]    findAll()
 * @method ClientOrigin[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ClientOriginRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry, EntityManagerInterface $manager)
    {
        parent::__construct($registry, ClientOrigin::class);
        $this->manager = $manager;
    }
}
