<?php

namespace App\Repository;

use App\Entity\Client;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @method Client|null find($id, $lockMode = null, $lockVersion = null)
 * @method Client|null findOneBy(array $criteria, array $orderBy = null)
 * @method Client[]    findAll()
 * @method Client[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ClientRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry, EntityManagerInterface $manager)
    {
        parent::__construct($registry, Client::class);
        $this->manager = $manager;
    }

    // public function add($firstname, $lastname, $dni, $celphone, $address, $email, $debe, $created_at, $updated_at)
    // {
    //     $entity = new Client();

    //     $entity
    //         ->setFirstname($firstname)
    //         ->setLastname($lastname)
    //         ->setDni($dni)
    //         ->setCelphone($celphone)
    //         ->setAddress($address)
    //         ->setEmail($email)
    //         ->setDebe($debe)
    //         ->setCreatedAt($created_at)
    //         ->setUpdatedAt($updated_at);

    //     $this->manager->persist($entity);
    //     $this->manager->flush();
    // }

    public function add(Client $entity)
    {
        $this->manager->persist($entity);
        $this->manager->flush();
    }

    public function update(Client $entity)
    {
        $this->manager->persist($entity);
        $this->manager->flush();

        // return $entity;
    }


    public function remove(Client $game)
    {
        $this->manager->remove($entity);
        $this->manager->flush();
    }
}
