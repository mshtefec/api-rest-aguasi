<?php

namespace App\Repository;

use App\Entity\OrderSupply;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @method OrderSupply|null find($id, $lockMode = null, $lockVersion = null)
 * @method OrderSupply|null findOneBy(array $criteria, array $orderBy = null)
 * @method OrderSupply[]    findAll()
 * @method OrderSupply[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OrderSupplyRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry, EntityManagerInterface $manager)
    {
        parent::__construct($registry, OrderSupply::class);
        $this->manager = $manager;
    }
}
