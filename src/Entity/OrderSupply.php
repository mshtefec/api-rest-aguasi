<?php

namespace App\Entity;

use App\Repository\OrderSupplyRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=OrderSupplyRepository::class)
 * @ORM\Entity
 * @ORM\Table(name="order_supply")
 */
class OrderSupply implements \JsonSerializable
{
    /**
     * @ORM\Id()
     * @ORM\ManyToOne(targetEntity="Order", inversedBy="supplies")
     * @ORM\JoinColumn(name="order_id", referencedColumnName="id", nullable=false)
     */
    private $orderId;

    /**
     * @ORM\Id()
     * @ORM\ManyToOne(targetEntity="Supply")
     * @ORM\JoinColumn(name="supply_id", referencedColumnName="id", nullable=false)
     */
    private $supplyId;

    /**
     * @ORM\Column(type="integer", length=11)
     */
    private $quantity;

    public function getOrder(): ?Order
    {
        return $this->orderId;
    }

    public function setOrder(?Order $orderId): self
    {
        $this->orderId = $orderId;

        return $this;
    }

    public function getSupply(): ?Supply
    {
        return $this->supplyId;
    }

    public function setSupply(?Supply $supplyId): self
    {
        $this->supplyId = $supplyId;

        return $this;
    }

    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    public function setQuantity(int $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function jsonSerialize(): array {
        return [
            'order' => $this->orderId,
            'supply' => $this->supplyId,
            'quantity' => $this->quantity
        ];
    }
}
