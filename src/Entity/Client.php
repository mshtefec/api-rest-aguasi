<?php

namespace App\Entity;

use App\Repository\ClientRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ClientRepository::class)
 */
class Client implements \JsonSerializable
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $firstname;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $lastname;

    /**
     * @ORM\Column(type="integer")
     */
    private $dni;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $celphone;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $address;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $province;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $locality;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    private $debe;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="seller", referencedColumnName="id")
     */
    private $seller;

    /**
     * @ORM\Column(type="datetime")
     */
    private $high_date;
    
    /**
     * @ORM\Column(type="datetime")
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updated_at;

    /**
     * @var int
     *
     * @ORM\OneToMany(targetEntity="Order",mappedBy="client", cascade={"all"}, orphanRemoval=true)
     */
    private $orders;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $cp;

    /**
     * @ORM\ManyToOne(targetEntity="Zone")
     * @ORM\JoinColumn(name="zone", referencedColumnName="id")
     */
    private $zone;

    /**
     * @ORM\ManyToOne(targetEntity="ClientStatus")
     * @ORM\JoinColumn(name="status", referencedColumnName="id")
     */
    private $status;

    /**
     * @ORM\ManyToOne(targetEntity="ClientOrigin")
     * @ORM\JoinColumn(name="origin", referencedColumnName="id")
     */
    private $origin;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $description;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->orders = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getDni(): ?int
    {
        return $this->dni;
    }

    public function setDni(int $dni): self
    {
        $this->dni = $dni;

        return $this;
    }

    public function getCelphone(): ?string
    {
        return $this->celphone;
    }

    public function setCelphone(string $celphone): self
    {
        $this->celphone = $celphone;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getDebe(): ?string
    {
        return $this->debe;
    }

    public function setDebe(string $debe): self
    {
        $this->debe = $debe;

        return $this;
    }

    public function getSeller(): ?User
    {
        return $this->seller;
    }

    public function setSeller(User $seller): self
    {
        $this->seller = $seller;

        return $this;
    }

    public function getHighDate(): ?\DateTimeInterface
    {
        return $this->high_date;
    }

    public function setHighDate(\DateTimeInterface $high_date): self
    {
        $this->high_date = $high_date;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(\DateTimeInterface $updated_at): self
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    /**
     * Add orders.
     *
     * @param \App\Entity\Order $orders
     *
     * @return Order
     */
    public function addOrder(\App\Entity\Order $order)
    {
        $this->orders[] = $order;

        return $this;
    }

    /**
     * Remove orders.
     *
     * @param \App\Entity\Order $orders
     */
    public function removeOrder(\App\Entity\Order $order)
    {
        $this->orders->removeElement($order);
    }

    /**
     * Get orders.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOrders()
    {
        return $this->orders;
    }

    public function getProvince(): ?string
    {
        return $this->province;
    }

    public function setProvince(string $province): self
    {
        $this->province = $province;

        return $this;
    }

    public function getLocality(): ?string
    {
        return $this->locality;
    }

    public function setLocality(string $locality): self
    {
        $this->locality = $locality;

        return $this;
    }

    public function getCp(): ?string
    {
        return $this->cp;
    }

    public function setCp(string $cp): self
    {
        $this->cp = $cp;

        return $this;
    }

    public function getZone(): ?Zone
    {
        return $this->zone;
    }

    public function setZone(?Zone $zone): self
    {
        $this->zone = $zone;

        return $this;
    }

    public function getStatus(): ?ClientStatus
    {
        return $this->status;
    }

    public function setStatus(?ClientStatus $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getOrigin(): ?ClientOrigin
    {
        return $this->origin;
    }

    public function setOrigin(?ClientOrigin $origin): self
    {
        $this->origin = $origin;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function jsonSerialize(): array {
        return [
            'id' => $this->id,
            'firstname' => $this->firstname,
            'lastname' => $this->lastname,
            'dni' => $this->dni,
            'celphone' => $this->celphone,
            'address' => $this->address,
            'province' => $this->province,
            'locality' => $this->locality,
            'email' => $this->email,
            'debe' => $this->debe,
            'seller' => $this->seller,
            'zone' => $this->zone,
            'high_date' => $this->high_date,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'orders' => $this->orders
        ];
    }
}
