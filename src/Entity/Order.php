<?php

namespace App\Entity;

use App\Repository\OrderRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=OrderRepository::class)
 * @ORM\Table(name="`order`")
 */
class Order implements \JsonSerializable
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;
    
    /**
     * @ORM\ManyToOne(targetEntity="Client")
     * @ORM\JoinColumn(name="client", referencedColumnName="id")
     */
    private $client;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="seller", referencedColumnName="id")
     */
    private $seller;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="delivery", referencedColumnName="id")
     */
    private $delivery;

    /**
     * @ORM\ManyToOne(targetEntity="Supply")
     * @ORM\JoinColumn(name="supply", referencedColumnName="id")
     */
    private $supply;

    /**
     * @var int
     *
     * @ORM\OneToMany(targetEntity="OrderSupply", mappedBy="orderId", cascade={"all"}, orphanRemoval=true)
     */
    private $supplies;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $address;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $status;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $quantity;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $total;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $description;

    /**
     * @ORM\Column(type="datetime")
     */
    private $delivery_date;

    /**
     * @ORM\Column(type="datetime")
     */
    private $load_date;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updated_at;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->supplies = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    public function setQuantity(int $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function getTotal(): ?string
    {
        return $this->total;
    }

    public function setTotal(string $total): self
    {
        $this->total = $total;

        return $this;
    }

    public function getClient(): ?Client
    {
        return $this->client;
    }

    public function setClient(?Client $client): self
    {
        $this->client = $client;

        return $this;
    }

    public function jsonSerialize(): array {
        return [
            'id' => $this->id,
            'client' => $this->client,
            'seller' => $this->seller,
            'delivery' => $this->delivery,
            'supply' => $this->supply,
            'address' => $this->address,
            'status' => $this->status,
            'quantity' => $this->quantity,
            'address' => $this->address,
            'total' => $this->total,
            'delivery_date' => $this->delivery_date,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'supplies' => $this->supplies
        ];
    }

    public function getSeller(): ?User
    {
        return $this->seller;
    }

    public function setSeller(?User $seller): self
    {
        $this->seller = $seller;

        return $this;
    }

    public function getDelivery(): ?User
    {
        return $this->delivery;
    }

    public function setDelivery(?User $delivery): self
    {
        $this->delivery = $delivery;

        return $this;
    }

    public function getSupply(): ?Supply
    {
        return $this->supply;
    }

    public function setSupply(?Supply $supply): self
    {
        $this->supply = $supply;

        return $this;
    }

    /**
     * Add supplies.
     *
     * @param \App\Entity\OrderSupply $supplies
     *
     * @return OrderSupply
     */
    public function addSupply(\App\Entity\OrderSupply $supply)
    {
        $this->supplies[] = $supply;

        return $this;
    }

    /**
     * Remove supplies.
     *
     * @param \App\Entity\OrderSupply $supplies
     */
    public function removeSupply(\App\Entity\OrderSupply $supply)
    {
        $this->supplies->removeElement($supply);
    }

    /**
     * Get supplies.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSupplies()
    {
        return $this->supplies;
    }

    public function clearSupplies()
    {
        $this->getSupplies()->clear();
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getDeliveryDate(): ?\DateTimeInterface
    {
        return $this->delivery_date;
    }

    public function setDeliveryDate(\DateTimeInterface $delivery_date): self
    {
        $this->delivery_date = $delivery_date;

        return $this;
    }

    public function getLoadDate(): ?\DateTimeInterface
    {
        return $this->load_date;
    }

    public function setLoadDate(\DateTimeInterface $load_date): self
    {
        $this->load_date = $load_date;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(\DateTimeInterface $updated_at): self
    {
        $this->updated_at = $updated_at;

        return $this;
    }
}
